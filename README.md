# Gerador de Links para WhatsApp

Programa para gerar links personalizados e facilitar o compartilhamento no WhatsApp.

![Tela inicial](screenshot-01.png)

![Tela com link gerado com número](screenshot-02.png)

![Tela com link gerado com número e mensagem](screenshot-03.png)

Projeto publicado em: [https://franciscochaves.gitlab.io/gerador-de-links-para-whatsapp/](https://franciscochaves.gitlab.io/gerador-de-links-para-whatsapp/)
